Bedelia
=======

Dominio del problema
---------------------

A partir de los protocolos de presencialidad el área de bedelia de la facultad necesita una aplicación para la asignación de aulas. 
Dada una configuración de aulas y un conjunto de pedidos de cursos, la aplicación debe asignar un aula a cada curso.

Las aulas se caracterizan por:
* Cantidad de asientos
* Cantidad de ventanas
* Tipo de bancos: pupitre_individual | pupitre_colectivo | mesa_de_trabajo

Tradicionalmente la capacidad de un aula está dada por la cantidad de asientos, pero según el protocolo vigente se aplican las siguientes restricciones de ventilación:

* si el aula no tiene ventanas su capacidad disminuye un 30%
* Si el aula tiene 1 o 2 ventanas entonces su capacidad disminuye un 10%
* Si el aula tiene más de 2 ventanas entonces su capacidad no disminuye

Adicionalmente hay restricciones de distanciamiento para las aulas de tipo pupitre_colectivo las cuales tienen una capacidad restringida del 50 %.

Por otro lado, cada pedido de aula incluye el detalle de características requeridas:

* Cantidad de alumnos / capacidad
* ¿Requiere mesa_de_trabajo? 
* Dia (ejemplo: lunes, martes, ...)
* Rango horario (ejemplo: de 19 a 22)


Funcionalidades a desarrollar
-----------------------------

A partir del contexto mencionado esto se debe construir una API HTTP/Rest que permita la siguientes operaciones.


### Creación de aula

````
pendiente
````

### Consulta de aulas

````
pendiente
````

### Pedido de aula

````
pendiente
````

### Reasginación de aulas

````
pendiente
````



### Reset
Esta funcionalidad vuelve el sistema a cero y es usadada solo a fines de prueba.
```
Request
POST /reset 

Response
200
{ "resultado": "ok"}
```
